package tommy;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;

public class Main {

  public static void main(String[] args) throws IOException, URISyntaxException {
    System.out.println("Put the size  for the input: ");
    Scanner sn = new Scanner(System.in);
    int n = sn.nextInt();
    Set<String> hashes = new HashSet<>();
    for (int i = 0; i < n; ++i) {
      System.out.println("Put the hash here: ");
      String in = sn.next();
      hashes.add(in);
    }

    Main main = new Main();

    main.readAndCheck(hashes);
  }


  private boolean isInInputArray(String hashedFromFile, Set<String> targets) {
    boolean isInArray = false;
    for (String target : targets) {
      if (target.equals(hashedFromFile)) {
        isInArray = true;
      }
    }
    return isInArray;
  }

  private void readAndCheck(Set<String> values) throws IOException, URISyntaxException {
    String fileName = "documents.0";
    URL fileUrl = getClass()
        .getClassLoader()
        .getResource(fileName);

    if (fileUrl == null) {
      throw new RuntimeException("Cannot load file " + fileName + " from classpath");
    }
    Path path = Paths
        .get(fileUrl.toURI());

    Stream<String> lines = Files.lines(path);
    lines.forEach(line -> {
      String hashedDoc = stringToSha256(line);
      if (isInInputArray(hashedDoc, values)) {
        System.out.println("Found! The DNI is" + line);
      }
      System.out.println(line + " not found");
    });
    lines.close();
  }

  private static String stringToSha256(final String input) {
    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-256");
      md.update(input.getBytes("UTF-8"));
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      e.printStackTrace(); // only in std io since any other approach is required.
      throw new RuntimeException("Errors during hashing function creation", e);
    } finally {
      if (md != null) {
        md.reset();
      }
    }

    return bytesToHex(md.digest());
  }

  private static String bytesToHex(byte[] b) {
    char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    StringBuilder strBuilder = new StringBuilder();
    for (byte aB : b) {
      strBuilder.append(hexDigit[(aB >> 4) & 0x0f]);
      strBuilder.append(hexDigit[aB & 0x0f]);
    }
    return strBuilder.toString();
  }
}
